import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

export default function CourseCard() {
  return (
    <Card>
     <Card.Body>
        <Card.Title>Sample Course</Card.Title>
        <h5>
          Description:
          </h5>
          <p>
          This is a sample course offering.
          </p>
          <h5>
          Price:
           </h5>
          <p>
          PHP 40,000
        </p>
        <Button variant="primary">Enroll</Button>
      </Card.Body>
    </Card>
  );
}

