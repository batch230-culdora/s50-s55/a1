import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Card from '../components/CourseCard';

export default function Home() {
	return(
		<Fragment>
			<Banner />
			<Highlights />
			<Card />
		</Fragment>
	)
}