// import logo from './logo.svg';
import './App.css';
import AppNavbar from './components/AppNavbar';
/*import Banner from './components/Banner';
import Highlights from './components/Highlights';*/
import Home from './pages/Home';
import { Container } from 'react-bootstrap';
import { Fragment } from 'react';

function App() {
  return (
    // 
    <Fragment>
    <AppNavbar/ >
    <Container>
    <Home />
   {/* <Banner/>
    <Highlights/>*/}
    </Container>
    </ Fragment>
  );
}

export default App;
